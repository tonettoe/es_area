cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (figure "src/polygon.cpp" "src/circle.cpp" "src/rectangle.cpp" "src/square.cpp" )

add_executable (test_figure test/test_figure.cpp)

target_link_libraries (test_figure figure)

enable_testing ()

add_test ("6 9" test_figure)
