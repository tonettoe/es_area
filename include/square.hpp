#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "rectangle.hpp"
using namespace std;
class Square : public Rectangle{

public:
	Square();
	Square(int);
};

#endif
