#ifndef FIGURE_HPP
#define FIGURE_HPP

using namespace std;

class Figure {

		
public:
	virtual float getArea() =0;

};

#endif
