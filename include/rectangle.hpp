#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "polygon.hpp"
using namespace std;
class Rectangle : public Polygon{
	int base,height;
public:
	Rectangle();
	Rectangle(int r,int f);
	virtual float getArea();
};	

#endif
