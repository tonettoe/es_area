#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "figure.hpp"
#include <math.h>

using namespace std;

class Circle : public Figure {
	int radius;
public:
	Circle();
	Circle(int r);
	float getArea();


};

#endif
