#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "figure.hpp"

using namespace std;

class Polygon : public Figure {
	int sides;
public:
	Polygon(int s);
	virtual float getArea() =0;
};

#endif
